//
//  SortingServiceProvider .swift
//  UI.SortingScanner
//
//  Created by Soslan-Bek on 21.11.2022.
//

import Foundation
import CoreData

import RxSwift
import RxCocoa

import Route4MeSDK

protocol SortingServiceProviderType {
	func takeOrder(by id: String) -> Order?
	
	func takeTerritory(by id: String) -> [Territory]
}

final class SortingServiceProvider {
	
	private enum DataKeys: String {
		case lastLoadOrders
		case lastLoadUpdatedOrders
	}
	
	// MARK: - Private properties
	private var _lastDownloadDate: String!
	private let _network: SortingNetworkProvider
	private let _dataManager: SortingCoreDataManager
	
	
	private let _concurrentQueue = DispatchQueue(label: "sorting-module.orders.load",
										qos: .default, attributes: .concurrent)
	
	// MARK: - Constructor
	init(network: SortingNetworkProvider, dataManager: SortingCoreDataManager) {
		self._network = network
		self._dataManager = dataManager
		
		
		// Check and set the last donwload date
		let date = UserDefaults.standard.loadDate(forKey: DataKeys.lastLoadOrders.rawValue)
		let timeIntervalDate = Int(date?.timeIntervalSince1970 ?? Date.getLastTwoWeekTimeInterval())

		let testDate = Int(Date.getLastTwoWeekTimeInterval())
		self._lastDownloadDate = "\(testDate)"//"\(timeIntervalDate)"
		
		// Check and saved territories
		checkLastUpdatedTerritories()
		
		// Load orders
		checkLastUpdatedOrders()
	}
	
	func updateCurrentOrder(_ order: UpdateOrderModel) -> Observable<Bool> {
		return Observable.create { [unowned self] observer in
			self._network.updateOrder(order) { result in
				switch result {
				case .success:
					observer.onNext(true)
				case .failure(let error):
					print(error.localizedDescription)
					observer.onNext(false)					
				}

                observer.onCompleted()
			}
			
			return Disposables.create()
		}
	}
}

// MARK: - SortingServiceProvider
extension SortingServiceProvider: SortingServiceProviderType {
	func takeOrder(by id: String) -> Order? {
		_dataManager.fetchOrder(by: id)
	}
	
	func takeTerritory(by id: String) -> [Territory] {
		_dataManager.fetchTerritory(by: id)
	}
}

// MARK: - Orders
private extension SortingServiceProvider {
	
	// 
	func checkLastUpdatedOrders() {
		let loadedOrders = self._dataManager.orders()
		
		guard !loadedOrders.isEmpty else {
			loadedAllOrders()
			return
		}
		
		loadedAllOrders()
		
		// If we have orders, we need first, delete old orders, after, load new orders and update
		
		// Delete old orders (which hold more than 2 weeks)
		self._dataManager.deleteOldsOrders()
	}
	
	/// This method load all orders include updated orders
	func loadedAllOrders() {
		let group = DispatchGroup()
		
		var orders: [SortingOrderModel] = []
		var updatedOrders: [SortingOrderModel] = []
		
		// Load orders
		group.enter()
		self._network.getCreatedOrders(lastDownload: self._lastDownloadDate) { result in
			switch result {
			case .success(let data):
				orders = data
                UserDefaults.standard.saveDate(Date(), forKey: DataKeys.lastLoadOrders.rawValue)
			case .failure(let error):
				print(error.localizedDescription)
			}
						
			group.leave()
		}
		
		// Load updated orders
		group.enter()
		self._network.getUpdatedOrders(lastDownload: self._lastDownloadDate) { result in
			switch result {
			case .success(let data):
				updatedOrders = data
                UserDefaults.standard.saveDate(Date(), forKey: DataKeys.lastLoadUpdatedOrders.rawValue)
			case .failure(let error):
				print(error.localizedDescription)
			}
						
			group.leave()
		}
		
		group.notify(queue: _concurrentQueue) {						
			// Need save orders
			self._dataManager.addOrdersToPersistent(orders)
		
			updatedOrders.forEach { self._dataManager.updateOrder($0) }			
		}
	}
}

// MARK: - Territories
private extension SortingServiceProvider {
	// Territories check
	func checkLastUpdatedTerritories() {
		let loadedTerritories = self._dataManager.territories()
		guard !loadedTerritories.isEmpty else {
			loadUserTerritoriesAndSaved()
			return
		}
		
		self._dataManager.delete(Territory.self) { result in
			self.loadUserTerritoriesAndSaved()
		}
	}
	
	// Load Territories from network and saved to db
	func loadUserTerritoriesAndSaved() {
		_network.getAllTerritories { [weak self] result in
			switch result {
			case .success(let territories):
				territories.forEach { self?._dataManager.addTerritoriesToPersistent($0) }
			case .failure(let error):
				print(error.localizedDescription)
			}
		}
	}
}
