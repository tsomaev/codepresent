//
//  FlightsListPopapView.swift
//  NDA
//
//  Created by Soslan-Bek Tsomaev on 05.03.2021.
//

import UIKit

final class FlightsListPopapViewController: BaseViewController {
    
    var dataSource: [FlightsListsResponseModel] = []
    var selectedFlightModel: ((FlightsListsResponseModel) -> Void)?
    
    // MARK: - Private properties 
    private var tableViewHeight: CGFloat = 0.0
    
    // MARK: - UI
    private let tableView: UITableView = {
        let tableView = UITableView()
        tableView.isScrollEnabled = true
        tableView.estimatedRowHeight = 100.0
        tableView.tableFooterView = UIView()
        tableView.backgroundColor = .white
        tableView.rowHeight = UITableView.automaticDimension
        return tableView
    }()
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        let width = 297
        let height = 166
        preferredContentSize = CGSize(width: width, height: height)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        setupUIViews()
    }
}

// MARK: - UITableView
extension FlightsListPopapViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        let item = dataSource[indexPath.row]
        
        cell.textLabel?.text = "\(item.number)"
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        selectedFlightModel?(dataSource[indexPath.row])
        dismiss(animated: true, completion: nil)
    }
}

// MARK: - Setup
private extension FlightsListPopapViewController {
    func setupUIViews() {
        view.addSubview(tableView)
        tableView.dataSource = self
        tableView.delegate = self
        tableView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }
}
