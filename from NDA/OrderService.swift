//
//  OrderService.swift
//  NDA
//
//  Created by Soslan-Bek Tsomaev on 29.03.2021.
//

import Foundation
import RxSwift
import RxCocoa

final class OrderService: OrderServiceType {
    
    // MARK: - Private properties
    private let bag = DisposeBag()
    
    private let cache: CacheManagerType
    private let websocket: WebSocketProviderType
    private let network: NetworkMainProviderType
    private let flightDataService: FlightsDataServiceType
    private let notificationService: LocalNotificationManagerType
    
    // MARK: - Visible properties
    var updatedOrdersTrigger = PublishRelay<Void>()
    var ordersArray = BehaviorRelay<[OrdersResponseModel]>(value: [])
    
    // MARK: - Constructor
    init(cache: CacheManagerType,
         network: NetworkMainProviderType,
         websocket: WebSocketProviderType,
         flightDataService: FlightsDataServiceType,
         notificationService: LocalNotificationManagerType) {
        self.cache = cache
        self.network = network
        self.websocket = websocket
        self.flightDataService = flightDataService
        self.notificationService = notificationService                
        
        /// Get orders list
        updatedOrdersTrigger.asObservable()
            .startWith(())
            .flatMap { [unowned self] guids -> Observable<[OrdersResponseModel]> in
                return self.getOrderListModel(guids: []).catchErrorJustReturn([])
            }
            .do(onNext: { [unowned self] response in
                self.cache.cache(response)
            })
            .bind(to: ordersArray)
            .disposed(by: bag)
        
        // Listening a orders
        websocket.input.socketEventsListen.asObservable()
            .subscribe { [weak self] event in
                guard let self = self else { return }
                switch event {
                case .orderModel(let model):
                    self.preparePushNotification(model)
                }
            } onError: { error in
                print(error.localizedDescription)
            }.disposed(by: bag)
        
        // Order's remove trigger
        NotificationCenter.default.addObserver(forName: .removedAllOrders,
                                               object: nil, queue: nil) { _ in
            self.ordersArray.accept([])
        }
    }
}

// MARK: - Input & Output
extension OrderService: OrderServiceInput, OrderServiceOutput {
    func completeOrder(guid: String, employeeId: Int) -> Completable {
        return network.completeOrder(guid: guid, employeeId: employeeId)
    }
    
    func denyOrder(guid: String, employeeId: Int, comment: String) -> Completable {
        return network.denyOrder(guid: guid, employeeId: employeeId, comment: comment)
    }
    
    func getOrderListModel(guids: [String]) -> Observable<[OrdersResponseModel]> {
        return network.getOrders(guids: guids)
            .asObservable()
    }    
}

// MARK: - Setup
private extension OrderService {
    func preparePushNotification(_ model: OrderDecodeModel) {
        self.getOrderListModel(guids: [model.guid])
            .compactMap { $0.first }
            .map { response -> [OrdersResponseModel] in
                var oldValues = self.ordersArray.value.filter { $0.guid != model.guid }
                oldValues.append(response)
                return oldValues
            }
            .subscribe { response in
                let getOrder = response.filter { $0.guid == model.guid }.first
                let passengerNameHash = getOrder?.nameHash ?? ""
                let passengerName = self.flightDataService.output.getPassengerName(by: passengerNameHash)
                
                let title = "Заказ с места \(model.seat)"
                let message = "Пассажир: \(passengerName). \(model.message)"
                self.notificationService.input.showNotifiacation(title: title, subtitle: message)
                
                self.ordersArray.accept(response)
                
                let applicationState = UIApplication.shared.applicationState
                guard applicationState == .background else { return }
                let guidData = ["guid": model.guid]
                NotificationCenter.default.post(name: .showCallController, object: nil, userInfo: guidData)
            } onError: { error in
                print(error.localizedDescription)
            }.disposed(by: bag)
    }
}
