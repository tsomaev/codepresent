//
//  RMOrderScanViewModel.swift
//  UI.SortingScanner
//
//  Created by Soslan-Bek on 04.10.2022.
//

import Foundation
import MapKit

import RxSwift
import RxCocoa

final class RMOrderScanViewModel {
	
	// MARK: - Private properties
	private let _bag = DisposeBag()
	private let _service: SortingServiceProvider
	
	private var _currentOrder: Order?
	private var _currentTerritory: [Territory]?
		
	// MARK: - Public
	/// Observable to configure bottom's buttons in the view
	var currentState = BehaviorRelay<RMSortingScanResultEnum>(value: .readyToScan)
	/// To this subject emiting a scanned barcode
	var scannedQRCode = PublishSubject<String>()
	/// Observable object to listen order's search result
	var searchResultObserver = PublishSubject<Result<[RMOrderFindStateViewEnum], RMSorintgOrderFindErrorType>>()
	
	var updateOrderTrigger = PublishSubject<Void>()
	
	// MARK: - Constructor
	init(service: SortingServiceProvider) {
		self._service = service				
		
		// Update order trigger
		updateOrderTrigger.asObservable()
			.compactMap { self._currentOrder }
			.map { self.prepareSendModel($0) }
			.flatMapLatest { order -> Observable<Bool> in
				return self._service.updateCurrentOrder(order)
			}
			.subscribe(onNext: { item in
				print("success")
			}, onError: { error in
				print(error.localizedDescription)
			}).disposed(by: _bag)
		
		scannedQRCode.asObservable()
			.observe(on: ConcurrentDispatchQueueScheduler(qos: .default))
			.flatMapLatest { service.takeOrder(by: $0) } // Take the orders with associate with barcode
			.compactMap { $0 }
			.flatMapLatest { takedOrder -> Observable<Result<[RMOrderFindStateViewEnum], RMSorintgOrderFindErrorType>> in
				
				// Take territories by the OrderID
				let territories = service.takeTerritory(by: "\(takedOrder.id)")
				
				// Preparing model to send to view where display address and color
				let territoriesModel: [RMSortingFindedOrderModel] = territories.map {
					RMSortingFindedOrderModel(territoryName: $0.name,
											  territoryColor: $0.color)
				}
				
				var scanResult: [RMOrderFindStateViewEnum] = []
				
//				let mapTerritories: [DetailTerritoryModel] = territories.map {
//					let territoryType = TerritoryTypeEnum(rawValue: $0.territory?.type ?? "") ?? .poly
//					return DetailTerritoryModel(type: territoryType,
//										 data: $0.territory?.data ?? [])
//				}
				
				let territoriesWithCoordinates: [SortingTerritoriesType] = territories.map {
					let territory = $0.territory
					return SortingTerritoriesType(type: territory?.type ?? "",
												  color: $0.color ?? "",
												  coordinates: territory?.data ?? [])
				}
				
				let sortTerritories: SortingTerritoryData = SortingTerritoryData(lat: takedOrder.cachedLat,
																				 long: takedOrder.cachedLng,
																				 territories: territoriesWithCoordinates)
				
				scanResult.append(.foundOrdersAndTerritories(data: territoriesModel))
				scanResult.append(.destination(address: takedOrder.firstAddress,
											   location: sortTerritories))
				
				// Set state for buttons
				if territoriesModel.isEmpty {
					self.currentState.accept(.orderWithoutTerritory)
				} else {
					self.currentState.accept(.orderAndTerritoryFound)
				}
				
				self._currentOrder = takedOrder
				self._currentTerritory = territories
				
				return .just(.success(scanResult))
			}
			.bind(to: searchResultObserver)
			.disposed(by: _bag)
	}
}

// MARK: - Private
private extension RMOrderScanViewModel {
	func prepareSendModel(_ currentModel: Order) -> UpdateOrderModel {
		let locationManager = CLLocationManager().location?.coordinate
		
		let sortedOnUTC: String = ""
		let sortedOnDate: String = ""
		let sortedLat: String = String(locationManager?.latitude ?? 0)
		let sortedLng: String = String(locationManager?.longitude ?? 0)
		
		let customDataModel: SortingCustomData = SortingCustomData(sortedLat: sortedLat,
																   barcode: currentModel.barcode ?? "",
																   sortedOnDate: sortedOnDate,
																   sortedLng: sortedLng,
																   sortedOnUTC: sortedOnUTC)
		
		return UpdateOrderModel(address1: currentModel.firstAddress ?? "",
								lastStatus: Int(currentModel.lastStatus),
								cachedLat: currentModel.cachedLat,
								cachedLng: currentModel.cachedLng,
								mergeCustomFields: false,
								orderID: Int(currentModel.id),
								dayScheduledForYYMMDD: "",
								extFIELDCustomData: customDataModel)
	}
}
