//
//  StatisticSubscriptionsManager.swift
//  MySubscribe
//
//  Created by Soslan-Bek Tsomaev on 11.02.2022.
//  Copyright © 2022 tawfik. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import StoreKit

final class StatisticSubscriptionsManager: StatisticSubscriptionsManagerType {
	
	var updateStatisticTrigger = PublishSubject<Void>()
	/// Observable to listent statistics
	var statistcsData = PublishSubject<TotalSubscriptionsInfo>()
	
	// MARK: - Private properties
	private let bag = DisposeBag()
	
	
	private let userSubsctipionsManager: UserSubscriptionsManagerType
	private var allSubscriptions = BehaviorRelay<[SubscriptionResponseModel]>(value: [])
	
	/// Уже потрачено
	private var totalExpenses: Float = 0.0
	/// Будет оплачено
	private var willBeExpenses: Float = 0.0
	/// Общее количество
	private var totalSubscriptionsCount: Int = 0
	
	// MARK: - Constructor
	init(userSubsctipionsManager: UserSubscriptionsManagerType) {
		self.userSubsctipionsManager = userSubsctipionsManager
		
		CurrencyListsManager.shared.currentUserCurrency.asObservable()
			.map { _ in }
			.bind(to: updateStatisticTrigger)
			.disposed(by: bag)
		
		updateStatisticTrigger.asObservable()
			.startWith(())
			.flatMapLatest { [unowned self] _ in self.userSubsctipionsManager.output.allUserSubsctipions }
			.do(onNext: { [unowned self] subscriptions in
				self.statistcsData.onNext(self.takeDataFromSubscriptions(subscriptions))
			})
				.bind(to: allSubscriptions)
				.disposed(by: bag)
				
				//        self.userSubsctipionsManager.output.allUserSubsctipions
				//            .do(onNext: { [unowned self] subscriptions in
				//                self.statistcsData.onNext(self.takeDataFromSubscriptions(subscriptions))
				//            })
				//            .bind(to: allSubscriptions)
				//            .disposed(by: bag)
				}
}

// MARK: - Private methods
private extension StatisticSubscriptionsManager {
	func convertToUserCurrencySimple(_ currency: String, value: Float) -> Float {
		let defaultCurrency = CurrencyListsManager.shared.currentUserCurrency.value
		let currencyConvertsLists = CurrencyListsManager.shared.convertedCurrency.value
		
		guard currency != defaultCurrency else {
			return Float(value)
		}
		
		// Взяли курс доллара к отношению рублю
		let usdValueToModelCurrency = currencyConvertsLists
			.filter { $0.currency == currency }
			.first?.value ?? 0.0
		
		let convertModelPrivceToUSD = Float(value) / usdValueToModelCurrency
		
		let userDefaultCurrency = currencyConvertsLists
			.filter { $0.currency == defaultCurrency }
			.first?.value ?? 0.0
		
		
		return convertModelPrivceToUSD * userDefaultCurrency
	}
	
	func convertToUserCurrency(_ model: SubscriptionResponseModel) -> Float {
		let defaultCurrency = CurrencyListsManager.shared.currentUserCurrency.value
		let currencyConvertsLists = CurrencyListsManager.shared.convertedCurrency.value
		
		guard model.currency != defaultCurrency else {
			return Float(model.price) ?? 0.0
		}
		
		// Взяли курс доллара к отношению рублю
		let usdValueToModelCurrency = currencyConvertsLists
			.filter { $0.currency == model.currency }
			.first?.value ?? 0.0
		
		let convertModelPrivceToUSD = Float(model.price) ?? 0.0 / usdValueToModelCurrency
		
		let userDefaultCurrency = currencyConvertsLists
			.filter { $0.currency == defaultCurrency }
			.first?.value ?? 0.0
		
		
		return convertModelPrivceToUSD * userDefaultCurrency
	}
	
	// Метод для создании общей модели
	func takeDataFromSubscriptions(_ data: [SubscriptionResponseModel]) -> TotalSubscriptionsInfo {
		
		// Total count
		let totalCount = data.count
		self.totalSubscriptionsCount = totalCount
		
		// Expenses cost
		let takeExpensedCost = self.takeExpensesSubscriptionsCost(data)
		self.totalExpenses = takeExpensedCost
		
		// Will expenses cost
		let willExpensesCost = self.willExpensesSubscriptionsCost(data)
		self.willBeExpenses = willExpensesCost
		
		return TotalSubscriptionsInfo(totalExpenses: takeExpensedCost, willBeExpenses: willExpensesCost, totalSubscriptionsCount: totalCount)
	}
	
	/// Did expenses this month
	func takeExpensesSubscriptionsCost(_ data: [SubscriptionResponseModel]) -> Float {
		return data
			.filter { $0.isActive ?? true }
			.map {
				let convertToFloat = Float($0.purchasesInThisMonth()) * (Float($0.price) ?? 0.0)
				return self.convertToUserCurrencySimple($0.currency, value: convertToFloat)
			}
			.reduce(0, +)
	}
	
	/// Will expenses this month
	func willExpensesSubscriptionsCost(_ data: [SubscriptionResponseModel]) -> Float {
		let currentDate = Date()
		
		return data
			.filter { $0.isActive ?? true }
			.filter { subscription in
				let convertedDate = Date.getDate(by: subscription.nextPaymentAt)
				guard currentDate.get(.year) == convertedDate.get(.year)
						&& currentDate.get(.month) == convertedDate.get(.month)
						// && currentDate.get(.day) > convertedDate.get(.day)
				else {
					return false
				}
				return true
			}
			.map {
				/// Длительность подписки
				let subscriptionCycleDayCount = Float(SubscriptionCycleDaysValue.getSubscriptiomCycleValueFromWord($0.duration))
				
				/// Дни в этом месяце
				let daysInThisMonth = Date().daysInMonth()
				
				/// Текущий день
				let remaningDays = Date().get(.day)
				
				/// Оставщиеся дни в этом месяце
				let lostDays: Float = Float(daysInThisMonth - remaningDays)
				
				/// Количество раз когда деньги будут списаны
				let preparingDays: Float = (lostDays / subscriptionCycleDayCount).rounded(.awayFromZero)
				
				// Конвертируем в дефолтную валюту
				let convertedToDefaultCurrencyPrice = self.convertToUserCurrency($0)
				let totalWillBeExpenses = convertedToDefaultCurrencyPrice * preparingDays.rounded(.up)
				
				return totalWillBeExpenses
			}
			.reduce(0, +)
	}
}
