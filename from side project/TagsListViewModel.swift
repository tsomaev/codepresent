//
//  TagsListViewModel.swift
//  MySubscribe
//
//  Created by Soslan-Bek Tsomaev on 23.01.2022.
//  Copyright © 2022 tawfik. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

final class TagsListViewModel {
    
    // MARK: - Private properties
    private let bag = DisposeBag()
    private let tagsService: TagsServiceType
    
    // MARK: - Public properties
    var removeTagTrigger = PublishRelay<Int>()
    var updateTagsTrigger = PublishRelay<Void>()
    var tableDataSource = BehaviorRelay<[UserTagsResponseModel]>(value: [])
    
    // MARK: - Constructor
    init(tagsService: TagsServiceType) {
        self.tagsService = tagsService
        
        tagsService.output.tagsLists.asObservable()
            .bind(to: tableDataSource)
            .disposed(by: bag)
   
        updateTagsTrigger.asObservable()
            .bind(to: tagsService.input.updateTagsList)
            .disposed(by: bag)
        
        removeTagTrigger.asObservable()
            .flatMapLatest { id -> Observable<Void> in
                return tagsService.input.removeTag(by: id)
            }
            .bind(to: updateTagsTrigger)
            .disposed(by: bag)
    }
}
