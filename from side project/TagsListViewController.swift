//
//  TagsListViewController.swift
//  MySubscribe
//
//  Created by Soslan-Bek Tsomaev on 23.01.2022.
//  Copyright © 2022 tawfik. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

final class TagsListViewController: UIViewController {
    
    var viewModel: TagsListViewModel!
    
    // MARK: - Private properties
    private let tableView: UITableView = {
        let tableView = UITableView()
        tableView.register(TagsSimpleCell.self)
        return tableView
    }()
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setup()
        navigationItem.title = "Tags"
    }
}

// MARK: - TableView Binding
extension TagsListViewController: UITableViewDelegate {
    func bindTableView() {
        viewModel.tableDataSource.asObservable()
            .bind(to: tableView.rx.items(cellIdentifier: TagsSimpleCell.identifier,
                                         cellType: TagsSimpleCell.self)) { row, item, cell in
                cell.tagsNameLabel.text = item.name
            }.disposed(by: bag)
        
        tableView.rx.itemDeleted
            .map { self.viewModel.tableDataSource.value[$0.row] }
            .map { $0.id }
            .bind(to: self.viewModel.removeTagTrigger)
            .disposed(by: bag)
    }
}

// MARK: - Private methods
private extension TagsListViewController {
    func setup() {
        tableViewSetup()
        bindTableView()
    }
    
    func tableViewSetup() {
        view.addSubview(tableView)
        tableView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }
}
