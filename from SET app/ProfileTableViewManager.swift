//
//  ProfileTableViewManager.swift
//  SET
//
//  Created by Soslan-Bek Tsomaev on 03.05.2021.
//  Copyright © 2021 tawfik. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxDataSources

protocol ProfileTableViewManagerDelegate: class {
    func logoutTapped()
    func nickNameChangeAlert()
}

final class ProfileTableViewManager: NSObject {
    
    weak var delegate: ProfileTableViewManagerDelegate?
    var dataSources: RxTableViewSectionedAnimatedDataSource<ProfileSectionModel>!
    
    // MARK: - Private Properties
    private var tableView: UITableView!
    private var viewModel: ProfileViewModelType!
    
    // MARK: - Constructor
    init(tableView: UITableView, viewModel: ProfileViewModelType) {
        self.tableView = tableView
        self.viewModel = viewModel
        
        super.init()
        tableViewBinding()
        tableView.rx.setDelegate(self).disposed(by: bag)
    }
    
    func tableViewBinding() {
        dataSources = RxTableViewSectionedAnimatedDataSource<ProfileSectionModel>(configureCell: { [unowned self] dataSource, tableView, indexPath, item in
            let item = self.dataSources[indexPath]
            
            switch item {
            case .userNickName:
                let cell = tableView.dequeue(UserNickNameCell.self, indexPath: indexPath)
                
                viewModel.output.userName
                    .bind(to: cell.userNickNameLabel.rx.text)
                    .disposed(by: cell.bag)
                
                return cell
                
            case .autoCarCell(let model):
                let cell = tableView.dequeue(AutoCarCell.self, indexPath: indexPath)            
                
                return cell
                
            case .addedAutoCell:
                let cell = tableView.dequeue(AddCarCell.self, indexPath: indexPath)
                return cell
                
            case .accountPhoneNumberCell:
                let cell = tableView.dequeue(AccountPhoneNumberCell.self, indexPath: indexPath)
                
                cell.goToPurchase.subscribe(onNext: { [weak self] _ in
                    self?.viewModel.input.goToPurchase()
                }).disposed(by: cell.bag)
                
                cell.logoutButton.rx.tap
                    .subscribe(onNext: { [weak self] _ in
                        self?.delegate?.logoutTapped()
                    }).disposed(by: cell.bag)
                                
                viewModel.output.userLikesCount
                    .map { "\($0)" }
                    .bind(to: cell.thanksCountLabel.rx.text)
                    .disposed(by: cell.bag)
                
                let phoneNumber = viewModel.output.userPhone.value
                let purchaseIsActivate = viewModel.output.purchaseIsActivate
                let model = AccountPhoneNumberCell.ViewModel(phoneNumber: phoneNumber,
                                                             purchaseIsActive: purchaseIsActivate)
                cell.fill(model)
                
                return cell
            }
        })
        
        dataSources.animationConfiguration = AnimationConfiguration(insertAnimation: .fade,
                                                                    reloadAnimation: .fade,
                                                                    deleteAnimation: .fade)
        
        viewModel.output.sectionModel.asObservable()
            .bind(to: tableView.rx.items(dataSource: dataSources))
            .disposed(by: bag)
        
        tableView.rx.itemSelected
            .map { (at: $0, animated: true) }
            .subscribe(onNext: tableView.deselectRow)
            .disposed(by: bag)
        
        tableView.rx.itemSelected.subscribe(onNext: { [weak self] indexPath in
            guard let item = self?.dataSources[indexPath] else { return }
            switch item {
            case .userNickName:
                self?.delegate?.nickNameChangeAlert()
            case .addedAutoCell:
                self?.viewModel.input.goToCarAdd()
            default: break
            }
        }).disposed(by: bag)
    }
}

// MARK: - UITableView Delegate
extension ProfileTableViewManager: UITableViewDelegate {
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView.viewFromNibName(CustomHeaderTitleView.identifier) as? CustomHeaderTitleView
        
        let title = dataSources[section].header
        headerView?.titleLabel.text = title
        
        if section != 0 {
            headerView?.backgroundColor = .blackHaze
        }
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        guard section != 0 else { return 0 }
        return 65.0
    }
}
